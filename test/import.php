<?php
if (PHP_SAPI != "cli") exit;
require_once '../FuzzySearch.php';

$mongo = new Mongo();
$ar = new FuzzySearch($mongo->animeTitle);
$lastword = '';
$lines = file('anime.txt');
$total = count($lines);
foreach($lines as $idx => $line){
	$s = trim($line);
	//タブから始まる行はエイリアス
	if ($line[0] == "\t"){
		$ar->addWordAlias($lastword, $s);
		$s = "  $s";
	} else {
		$ar->addWord($s);
		$lastword = $s;
	}
	printf("(%5d/%5d) %s\n", $idx+1, $total, $s);
}
